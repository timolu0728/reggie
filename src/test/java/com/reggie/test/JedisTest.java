package com.reggie.test;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;
import redis.clients.jedis.Jedis;

@SpringBootTest
@RunWith(SpringRunner.class)
public class JedisTest {

    @Autowired
    private RedisTemplate redisTemplate;

    @Test
    public void testJedis() {
        Jedis jedis = new Jedis("localhost", 6379);
        jedis.set("username","timo");
        System.out.println(jedis.get("username"));
        jedis.close();
    }

    @Test
    public void testRedisTemplate() {
        redisTemplate.opsForValue().set("username", "choi");
        String result = (String) redisTemplate.opsForValue().get("username");
        System.out.println(result);
    }
}
