package com.reggie.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.reggie.ddo.Orders;

public interface OrderService extends IService<Orders> {

    /**
     * 用户下单
     * @param orders
     */
    public void submit(Orders orders);

    public void setStatus(Orders orders);
}
