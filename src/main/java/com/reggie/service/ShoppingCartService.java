package com.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.reggie.ddo.ShoppingCart;

public interface ShoppingCartService extends IService<ShoppingCart> {

}
