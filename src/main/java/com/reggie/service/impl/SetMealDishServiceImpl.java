package com.reggie.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.reggie.dao.SetmealDishDao;
import com.reggie.ddo.SetmealDish;
import com.reggie.service.SetMealDishService;
import org.springframework.stereotype.Service;

@Service
public class SetMealDishServiceImpl extends ServiceImpl<SetmealDishDao, SetmealDish> implements SetMealDishService {

}
