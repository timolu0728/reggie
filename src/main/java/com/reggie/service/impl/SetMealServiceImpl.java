package com.reggie.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.reggie.common.CustomExcetion;
import com.reggie.common.MerchantConst;
import com.reggie.dao.SetmealDao;
import com.reggie.ddo.DishFlavor;
import com.reggie.ddo.Setmeal;
import com.reggie.ddo.SetmealDish;
import com.reggie.dto.SetmealDto;
import com.reggie.service.SetMealDishService;
import com.reggie.service.SetMealService;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


@Service
public class SetMealServiceImpl extends ServiceImpl<SetmealDao, Setmeal> implements SetMealService {

    @Autowired
    private SetMealDishService setMealDishService;

    @Override
    public void saveWithDish(SetmealDto setmealDto) {
        this.save(setmealDto);
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        Long setmealId = setmealDto.getId();
        setmealDishes.stream().map(setmealDish -> {
            setmealDish.setSetmealId(setmealId);
            return setmealDish;
        }).collect(Collectors.toList());

        setMealDishService.saveBatch(setmealDishes);
    }

    @Override
    public void updateWithDish(SetmealDto setmealDto) {
        this.updateById(setmealDto);

        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(SetmealDish::getSetmealId, setmealDto.getId());
        setMealDishService.remove(queryWrapper);

        if (null == setmealDto.getSetmealDishes() || setmealDto.getSetmealDishes().size() == 0) {
            return;
        }
        List<SetmealDish> setmealDishes = setmealDto.getSetmealDishes();
        Long setmealId = setmealDto.getId();
        setmealDishes.stream().map((flavor) -> {
            flavor.setSetmealId(setmealId);
            return flavor;
        }).collect(Collectors.toList());

        setMealDishService.saveBatch(setmealDishes);
    }

    @Override
    public SetmealDto getByIdWithDish(Long id) {
        Setmeal setMeal = this.getById(id);

        LambdaQueryWrapper<SetmealDish> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.eq(SetmealDish::getSetmealId, id);
        List<SetmealDish> flavors = setMealDishService.list(queryWrapper);

        SetmealDto setmealDto = new SetmealDto();
        BeanUtils.copyProperties(setMeal, setmealDto);
        setmealDto.setSetmealDishes(flavors);
        return setmealDto;
    }

    @Override
    @Transactional
    public void removeWithDish(List<Long> ids) {
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(Setmeal::getId, ids);
        queryWrapper.eq(Setmeal::getStatus, MerchantConst.COMMOM_YES);

        int count = this.count(queryWrapper);
        if (count > 0) {
            throw new CustomExcetion("Setmeal is in sale");
        }
        this.removeByIds(ids);

        LambdaQueryWrapper<SetmealDish> dishLambdaQueryWrapper = new LambdaQueryWrapper<>();
        dishLambdaQueryWrapper.in(SetmealDish::getSetmealId, ids);
        setMealDishService.remove(dishLambdaQueryWrapper);
    }

    @Override
    public void setStatus(List<Long> ids, int status) {
        LambdaQueryWrapper<Setmeal> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.in(Setmeal::getId, ids);

        int count = this.count(queryWrapper);
        if (count == 0) {
            throw new CustomExcetion("Setmeal doesn't exist");
        }

        List<Setmeal> setmealList = this.list(queryWrapper);
        setmealList.stream().map(setmeal -> {
            setmeal.setStatus(status);
            return setmeal;
        }).collect(Collectors.toList());

        this.updateBatchById(setmealList);
    }
}
