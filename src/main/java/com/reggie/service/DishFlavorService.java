package com.reggie.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.reggie.ddo.DishFlavor;

public interface DishFlavorService  extends IService<DishFlavor> {

}
