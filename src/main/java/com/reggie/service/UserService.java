package com.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.reggie.ddo.User;


public interface UserService extends IService<User> {
}
