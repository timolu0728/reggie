package com.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.reggie.ddo.Setmeal;
import com.reggie.dto.SetmealDto;
import java.util.List;

public interface SetMealService extends IService<Setmeal> {

    public void saveWithDish(SetmealDto setmealDto);

    public SetmealDto getByIdWithDish(Long id);

    public void removeWithDish(List<Long> ids);

    public void updateWithDish(SetmealDto setmealDto);

    public void setStatus(List<Long> ids, int status);
}
