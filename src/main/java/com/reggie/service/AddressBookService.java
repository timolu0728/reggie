package com.reggie.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.reggie.ddo.AddressBook;


public interface AddressBookService extends IService<AddressBook> {

}
