package com.reggie.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.reggie.ddo.Setmeal;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface SetmealDao extends BaseMapper<Setmeal> {

}
