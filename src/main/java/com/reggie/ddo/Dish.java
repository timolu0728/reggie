package com.reggie.ddo;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;

/**
 菜品
 */
@Data
public class Dish implements Serializable {

    protected static final long serialVersionUID = 1L;

    protected Long id;


    //菜品名称
    protected String name;


    //菜品分类id
    protected Long categoryId;


    //菜品价格
    protected BigDecimal price;


    //商品码
    protected String code;


    //图片
    protected String image;


    //描述信息
    protected String description;


    //0 停售 1 起售
    protected Integer status;


    //顺序
    protected Integer sort;


    @TableField(fill = FieldFill.INSERT)
    protected LocalDateTime createTime;


    @TableField(fill = FieldFill.INSERT_UPDATE)
    protected LocalDateTime updateTime;


    @TableField(fill = FieldFill.INSERT)
    protected Long createUser;


    @TableField(fill = FieldFill.INSERT_UPDATE)
    protected Long updateUser;


    //是否删除
    protected Integer isDeleted;

}
