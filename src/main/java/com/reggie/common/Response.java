package com.reggie.common;

import lombok.Data;
import java.util.HashMap;
import java.util.Map;

@Data
public class Response<T> {

    private Integer code; //编码：1成功，0和其它数字为失败

    private String msg; //错误信息

    private T data; //数据

    private Map map = new HashMap(); //动态数据

    public static <T> Response<T> success(T object) {
        Response<T> response = new Response<T>();
        response.data = object;
        response.code = 1;
        return response;
    }

    public static <T> Response<T> error(String msg) {
        Response response = new Response();
        response.msg = msg;
        response.code = 0;
        return response;
    }

    public Response<T> add(String key, Object value) {
        this.map.put(key, value);
        return this;
    }

}
