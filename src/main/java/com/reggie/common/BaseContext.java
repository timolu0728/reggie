package com.reggie.common;

/**
 * 基于ThreadLocal封装工具类，用户保存喝获取当前登录用户id
 */
public class BaseContext {


    public static Long employeeId;

    public static Long userId;

//    private static ThreadLocal<Long> threadLocal = new ThreadLocal<>();

    public static void setEmployeeId(Long id) {
//        threadLocal.set(id);
        employeeId = id;
    }

    public static Long getUserId() {
//        return threadLocal.get();
        return userId;
    }

    public static Long getEmployeeId() {
//        return threadLocal.get();
        return employeeId;
    }


    public static void setUserId(Long id) {
//        threadLocal.set(id);
        userId = id;
    }
}
