package com.reggie.common;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * 全局异常处理
 */
@ResponseBody
@Slf4j
@ControllerAdvice(annotations = {RestController.class, Controller.class})
public class GlobalExcationHandler {

    @ExceptionHandler(CustomExcetion.class)
    public Response<String> exceptionHandler(CustomExcetion ex) {
        log.error(ex.getMessage());
        return Response.error(ex.getMessage());
    }
}
