package com.reggie.dto;


import com.reggie.ddo.Setmeal;
import com.reggie.ddo.SetmealDish;
import lombok.Data;
import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;

    private String categoryName;
}
