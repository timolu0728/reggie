package com.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.reggie.common.Response;
import com.reggie.ddo.Employee;
import com.reggie.service.EmployeeService;
import java.time.LocalDateTime;
import javax.servlet.http.HttpServletRequest;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/employee")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @RequestMapping("/login")
    public Response<Employee> login(HttpServletRequest request, @RequestBody Employee employee) {

        String password = employee.getPassword();
        password = DigestUtils.md5DigestAsHex(password.getBytes());

        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(Employee::getUsername, employee.getUsername());
        //唯一
        Employee employee1 = employeeService.getOne(queryWrapper);

        if (null == employee1) {
            return Response.error("login failed");
        }

        if (!employee1.getPassword().equals(password)) {
            return Response.error("password eeeor");
        }

        if (employee1.getStatus() != 1) {
            return Response.error("account banned");
        }

        //用户id放到session
        request.getSession().setAttribute("employee", employee1.getId());
        return Response.success(employee1);
    }

    @RequestMapping("/logout")
    public Response<String> logout(HttpServletRequest request) {
        request.getSession().removeAttribute("employee");
        return Response.success("logout success");
    }

    @PostMapping
    public Response<String> save(HttpServletRequest request, @RequestBody Employee employee) {
        log.info("{}", employee.toString());

        employee.setPassword(DigestUtils.md5DigestAsHex("123456".getBytes()));
        employeeService.save(employee);

        return Response.success("save employee successfully");
    }

    @GetMapping("/page")
    public Response<Page> page(int page, int pageSize, String name) {

        Page pageInfo = new Page(page, pageSize);
        LambdaQueryWrapper<Employee> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.like(StringUtils.isNotEmpty(name), Employee::getName, name);
        queryWrapper.orderByDesc(Employee::getUpdateTime);

        employeeService.page(pageInfo, queryWrapper);

        return Response.success(pageInfo);
    }

    @PutMapping
    public Response<String> update(HttpServletRequest request, @RequestBody Employee employee) {

        Long empId = (Long) request.getSession().getAttribute("employee");
        log.info("employee id:{}", empId);
        employeeService.updateById(employee);

        return Response.success("update employee successfully");
    }

    @GetMapping("/{id}")
    public Response<Employee> getById(@PathVariable Long id) {
        Employee employee = employeeService.getById(id);
        if (null == employee) {
            return Response.error("employee invalid");
        }
        return Response.success(employee);
    }
}
