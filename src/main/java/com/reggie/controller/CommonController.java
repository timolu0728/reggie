package com.reggie.controller;

import com.reggie.common.Response;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.UUID;
import javax.servlet.MultipartConfigElement;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.MultipartConfigFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@Slf4j
@RestController
@RequestMapping("/common")
public class CommonController {

    @Value("${reggie.path}")
    private String baseaPath;

    @PostMapping("/upload")
    public Response<String> upload(MultipartFile file) {
        log.info(file.toString());

        File file1 = new File(baseaPath);
        if (!file1.exists()) {
            file1.mkdirs();
        }

        String fileName = UUID.randomUUID().toString() + file.getOriginalFilename()
                .substring(file.getOriginalFilename().lastIndexOf("."));
        try {
            file.transferTo(new File(baseaPath + fileName));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return Response.success(fileName);
    }

    @GetMapping("/download")
    public void dowbload(String name, HttpServletResponse response) {
        try {
            //输入流，通过输入流读取文件内容
            FileInputStream fileInputStream = new FileInputStream((new File(baseaPath + name)));
            //输出流，通过输出流将文件写回浏览器，在浏览器展示图片
            ServletOutputStream outputStream = response.getOutputStream();
            response.setContentType("image/jpeg");
            int len = 0;
            byte[] bytes = new byte[1024];
            while ((len = fileInputStream.read(bytes)) != -1) {
                outputStream.write(bytes,0,len);
                outputStream.flush();
            }
            fileInputStream.close();
            outputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Bean
    MultipartConfigElement multipartConfigElement() {
        MultipartConfigFactory factory = new MultipartConfigFactory();
        factory.setLocation(baseaPath);
        return factory.createMultipartConfig();
    }
}
