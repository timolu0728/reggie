package com.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.reggie.common.Response;
import com.reggie.ddo.Category;
import com.reggie.ddo.Employee;
import com.reggie.service.CategoryService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

//    public Response<Category> getCategory() {
//        return null;
//    }

    @PostMapping
    public Response<String> save(@RequestBody Category category) {
        categoryService.save(category);
        return Response.success("save category successfully");
    }

    @GetMapping("/page")
    public Response<Page> page(int page, int pageSize) {
        Page<Category> pageInfo = new Page(page, pageSize);
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.orderByAsc(Category::getSort);

        categoryService.page(pageInfo, queryWrapper);

        return Response.success(pageInfo);
    }

    @DeleteMapping
    public Response<String> delete(Long ids) {
        categoryService.remove(ids);
        return Response.success("delete category successfully");
    }

    @PutMapping
    public Response<String> update(@RequestBody Category category) {
        categoryService.updateById(category);
        return Response.success("update category successfully");
    }

    @GetMapping("/list")
    public Response<List<Category>> list(Category category) {
        LambdaQueryWrapper<Category> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(category.getType() != null, Category::getType, category.getType());
        queryWrapper.orderByAsc(Category::getSort).orderByDesc(Category::getUpdateTime);
        List<Category> list = categoryService.list(queryWrapper);
        return Response.success(list);
    }
}
