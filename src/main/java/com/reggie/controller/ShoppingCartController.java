package com.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.reggie.common.BaseContext;
import com.reggie.common.CustomExcetion;
import com.reggie.common.Response;
import com.reggie.ddo.ShoppingCart;
import com.reggie.service.ShoppingCartService;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/shoppingCart")
public class ShoppingCartController {


    @Autowired
    private ShoppingCartService shoppingCartService;

    @PostMapping("/add")
    public Response<ShoppingCart> add(@RequestBody ShoppingCart shoppingCart) {

        Long userId = BaseContext.getUserId();
        shoppingCart.setUserId(userId);
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, userId);

        Long dishId = shoppingCart.getDishId();
        Long setmealId = shoppingCart.getSetmealId();
        if (dishId != null) {
            queryWrapper.eq(ShoppingCart::getDishId, dishId);
        } else if (setmealId != null) {
            queryWrapper.eq(ShoppingCart::getSetmealId, setmealId);
        }
        ShoppingCart dbShoppingCart = shoppingCartService.getOne(queryWrapper);

        if (dbShoppingCart != null) {
            int number = dbShoppingCart.getNumber();
            dbShoppingCart.setNumber(number + 1);
            shoppingCartService.updateById(dbShoppingCart);
        } else {
            shoppingCart.setNumber(1);
            shoppingCartService.save(shoppingCart);
            dbShoppingCart = shoppingCart;
        }

        return Response.success(dbShoppingCart);
    }

    @GetMapping("/list")
    public Response<List<ShoppingCart>> list() {
        Long userId = BaseContext.getUserId();
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, userId);
        queryWrapper.orderByDesc(ShoppingCart::getCreateTime);
        List<ShoppingCart> list = shoppingCartService.list(queryWrapper);

        return Response.success(list);
    }

    @DeleteMapping("/clean")
    public Response<String> clean() {
        Long userId = BaseContext.getUserId();
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, userId);
        shoppingCartService.remove(queryWrapper);

        return Response.success("clean shopping cart successfully");
    }

    @PostMapping("/sub")
    public Response<ShoppingCart> sub(@RequestBody ShoppingCart shoppingCart) {
        Long userId = BaseContext.getUserId();
        shoppingCart.setUserId(userId);
        LambdaQueryWrapper<ShoppingCart> queryWrapper = new LambdaQueryWrapper<>();
        queryWrapper.eq(ShoppingCart::getUserId, userId);

        Long dishId = shoppingCart.getDishId();
        Long setmealId = shoppingCart.getSetmealId();
        if (dishId != null) {
            queryWrapper.eq(ShoppingCart::getDishId, dishId);
        } else if (setmealId != null) {
            queryWrapper.eq(ShoppingCart::getSetmealId, setmealId);
        }
        ShoppingCart dbShoppingCart = shoppingCartService.getOne(queryWrapper);

        if (dbShoppingCart != null) {
            int number = dbShoppingCart.getNumber();
            if (number > 1) {
                dbShoppingCart.setNumber(number - 1);
                shoppingCartService.updateById(dbShoppingCart);
            } else {
                shoppingCartService.removeById(dbShoppingCart);
            }
        } else {
            throw new CustomExcetion("shopping cart is invalid");
        }

        return Response.success(dbShoppingCart);
    }
}
