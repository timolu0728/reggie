package com.reggie.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.reggie.common.BaseContext;
import com.reggie.common.Response;
import com.reggie.ddo.Orders;
import com.reggie.service.OrderService;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 订单
 */
@Slf4j
@RestController
@RequestMapping("/order")
public class OrderController {


    @Autowired
    private OrderService orderService;

    @PostMapping("/submit")
    public Response<String> submit(@RequestBody Orders orders) {
        orderService.submit(orders);
        return Response.success("order submit successfully");
    }

    @GetMapping("/userPage")
    public Response<Page> userPage(int page, int pageSize) {
        Page<Orders> pageInfo = new Page(page, pageSize);
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.orderByDesc(Orders::getOrderTime);
        queryWrapper.eq(Orders::getUserId, BaseContext.getUserId());

        orderService.page(pageInfo, queryWrapper);
        return Response.success(pageInfo);
    }


    @GetMapping("/page")
    public Response<Page> page(int page, int pageSize,
            @Nullable String number, @Nullable String beginTime, @Nullable String endTime) {
        Page<Orders> pageInfo = new Page(page, pageSize);
        LambdaQueryWrapper<Orders> queryWrapper = new LambdaQueryWrapper();
        queryWrapper.orderByDesc(Orders::getOrderTime);
        if (null != number) {
            queryWrapper.eq(Orders::getNumber, number);
        }
        DateTimeFormatter df = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        if (null != beginTime) {
            LocalDateTime localDateTime = LocalDateTime.parse(beginTime, df);
            queryWrapper.ge(Orders::getOrderTime, localDateTime);
        }
        if (null != endTime) {
            LocalDateTime localDateTime = LocalDateTime.parse(endTime, df);
            queryWrapper.le(Orders::getOrderTime, localDateTime);
        }
        orderService.page(pageInfo, queryWrapper);
        return Response.success(pageInfo);
    }

    @PutMapping
    public Response<String> setStatus(@RequestBody Orders orders) {
        orderService.setStatus(orders);
        return Response.success("set order status successfully");
    }

}